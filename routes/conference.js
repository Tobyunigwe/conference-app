const path = require('path');
const db = require('../utils/database');
const express = require('express');
const conferencesRoutes = require('../controllers/conferences');
const router = express.Router();

//allConferences
router.get('/', conferencesRoutes.getAllconferences);

//showOneConference
router.get('/show/:conferenceId', conferencesRoutes.getAddConference);


//updateConference
// router.get('/update/:conferenceId', conferencesRoutes.getAddConference);
// router.post('/add-conference', conferencesRoutes.postStoreConference);

//addConference
router.get('/add-conference', conferencesRoutes.getAddConference);
router.post('/add-conference', conferencesRoutes.postStoreConference);

//AddUser
router.get('/add-conference', conferencesRoutes.getAddConference);
router.post('/add-conference', conferencesRoutes.postStoreConference);

module.exports = router;