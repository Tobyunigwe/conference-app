//Node modules
const path = require('path');
const bodyParser = require('body-parser');

// database
const db = require('./utils/database');

// models import
const Conference = require('./models/Conference');
const User = require('./models/User');

//Middleware
const express = require('express');

const app = express();

//import routes
const userRoutes = require('./routes/users');
const conferenceRoutes = require('./routes/conference');
// view engine setup
app.set('view engine', 'ejs');

app.use(bodyParser.urlencoded( // routes 7-12
    {extended: false})
);
//static files
app.use(express.static(path.join(__dirname,'public')));

app.use('/users', userRoutes);
app.use('/conference', conferenceRoutes);

app.get('/', (req, res) => {
    res.render('index', {
        title: 'Conference App',
        description: 'What can you do here?'
    });
});



app.use((req, res, next) => {
    res.status(404).render('404', {
        title: 404
    });
    console.log('app.js 404');
});

//testing sequelize
db.authenticate()
    .then(() => console.log('Database connected...'))
    .catch(err => console.log('Error: ' + err));

db.sync()
.then(result=> {
    console.log(result);
    app.listen(10056); // only start when
                      // .then() is reached.
})
    .catch(err => {
        console.log(err);
    });
