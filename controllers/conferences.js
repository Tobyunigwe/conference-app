const Conference = require('../models/Conference');

exports.getAllconferences = (req, res, next) => {
    const conferences = [
        {
            name: 'Laravel',
            place: 'Amsterdam'
        },
        {
            name: 'Nodejs',
            place: 'New York'
        },
        {
            name: 'Sequelize',
            place: 'Barcelona'
        }
    ];
    res.render('conference/index', {
        path: '/conference',
        title: 'All conferences',
        description: 'What conferences are upcoming?',
        conferences: conferences
    });
};

exports.getAddConference = (req, res, next) => {
    res.render('conference/addConference', {
        title: 'Add conference',
        description: 'What kind of conferences do you want to register?'
    });
};

exports.getConference = (req,res, next) => {
    const conferenceId = req.params.conferenceId;
    Conference.findByPk(conferenceId)
        .then(conference => {
            res.render('conference/show', {
                path: '/conference',
                title: 'One conferences',
                description: 'What conference are upcoming',
                conference: conference
            });
        })
        .catch(err => {
            console.log(err);
        });
};










exports.postStoreConference = (req, res, next) => {
    res.redirect('/conference');
    //const title = req.body.title;

Conference.create({
    title: title,
    description: description,
    size: size,
    dateStart: dateStart,
    dateEnd: dateEnd,
    confTown: confTown
}).then(result =>{
    console.log('Conference created');
}).catch(err => {
    console.log(err);
});

};

