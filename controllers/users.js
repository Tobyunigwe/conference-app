exports.getAllUsers = (req, res, next) => {
    const users = ['Rudy', 'Matthijs', 'Stephan'];
    res.render('users/index', {
        path: '/users',
        title: 'All Users',
        description: 'What function does everybody has?',
        users: users,
        add: 'Add User',
        link: 'users/add-user'
    });
};

exports.getAddUsers = (req, res, next) => {
    res.render('users/addUser', {
        path: '/add-users',
    });
};