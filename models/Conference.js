//sequelize
const Sequelize = require('sequelize');
//import database
const db = require('../utils/database');

const Conference = db.define('conference', {
    id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
    },
    title: {
        type: Sequelize.STRING,
        allowNull: false,
    },
    description: {
        type: Sequelize.TEXT,
        allowNull: false,
    },
    company: {
        type: Sequelize.STRING,
        allowNull: true,
    },
    size: {
        type: Sequelize.INTEGER,
        allowNull: true,
    },
    dateStart: {
        type: Sequelize.DATE,
        allowNull: false,
    },
    dateEnd: {
        type: Sequelize.DATE,
        allowNull: false,
    }

});

module.exports = Conference;

