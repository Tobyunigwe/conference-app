
//sequelize
const Sequelize = require('sequelize');
//import database
const db = require('../utils/database');

// modelsaanvullen
// elo meer info aanvullen https://elo.windesheim.nl/Start.aspx#-204
// mee info datatypes https://sequelize.org/v5/manual/data-types.html
const User = db.define('user', {
    id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
    },
    name: {
        type: Sequelize.STRING,
        allowNull: false,
    },
    email: {
        type: Sequelize.STRING,
        allowNull: false,
    },
    password: {
        type: Sequelize.STRING,
        allowNull: false,
    }
});

module.exports = User;

